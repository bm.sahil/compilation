# Model compilation setup Guide
## Introduction
This document guides the developers on “How to compile the Deep Learning model
with TVM Compiler”. The model is compiled from ONNX to TVM compiled model. The
compiled model is to demonstrate the capabilities of the ARM platform to run heavy
deep learning inference on low-cost CPU of ARM. To demonstrate this, the YoloV3-
based object detection is implemented to solve the traffic monitoring use case and is
deployed on the low-cost ARM Based CPU.


## Pre-Requisites
- ARM Based Embedded Platform. 
- USB Pen Drive Flashed with ARM System-Ready EWOAL Image.
- Docker pre-installed on EWOAL image.
- Setup Alibaba Cloud Service Account with OSS service enabled.
- Setup AWS cloud service account with s3 service enabled.


## Setup Guide
- Clone the project
Run the command given below to clone the project.
```sh
$ git clone https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/ml-model/compilation.git
```
- Directory Structure for cloned project
```sh
<PROJECT_ROOT>
  |_docs/
    |_ARM-Smart_camera_CSP_TVM_Compilation.pdf
    |_ARMCSP-API_Documentation_for_Model_Compilation.pdf
  |_src/
    |_onnx_to_tvm.py
    |_tar_export.py
    |_aws_agent.py
    |_argconfig.py
    |_alibaba_agent.py
  |_config/
    |_alibaba_config.json
    |_Aws_config.json
  |_Dockerfile
  |_requirements.txt
  |_main.py
  |_LICENSE
  |_trained_model/
  |_README.md
```

## Compiling the model and storing it on Alibaba cloud
### Creating RAM (Resource Access Management) User
A Resource Access Management (RAM) user is an entity created on Alibaba Cloud to
allow end user to interact with Alibaba Cloud Services and authorize different access
and permissions. Please Follow the below steps to setup the RAM.
- Open the Alibaba Cloud Page & Login with the Account Created.
- Search for ‘Resource Access Management’ on Alibaba Cloud console.
- Click on ‘Resource Access Management’ from the search result, it will open the Resource Access Management console.
- On the left-side navigation pane, choose “Identities > Users”.
- On the Users page, click on “Create User”.
- In the User Account Information section of the Create User page,
  - Enter the New Logon Name and Display Name parameters.
  - In the Access Mode section, select the access mode mentioned:
    - Console Access
      - Select this option and complete the logon security settings.
      - Select system-generated password.
      - Select password reset not required
      - Select multi-factor authentication (MFA) not required.
    - OpenAPI Access
      - Upon selection, an AccessKey pair is automatically created for the RAM user.
      - The RAM user can call API operations or use other
development tools to access Alibaba Cloud resources.
- After Configuring the Create User form, Click on OK.
- Download CSV file which consists of accesskey and secretkey.
- Click on the “Return” button.
- In the Actions column of the newly created user, click on “Add Permissions”.
- Under “Select Policies”, Click on “Enter policy name” box selectAliyunOSSFullAccess.
- Click on OK and then Complete button.

### Creating OSS (Object Storage Service) Bucket
Object Storage Service (OSS) stores data as objects within buckets. Please Follow the
below steps to setup the OSS,
- Search for ‘Object Storage Service’ on Alibaba Cloud console.
- Click on the ‘Object Storage Service’ from search result, it will open the Object
Storage Service console.
- In the left-side navigation pane, click on Buckets and click Create Bucket on
bucket home page.
- In the Create Bucket panel, configure the required parameters like the name of
bucket, and the region.
- Select preferred region for OSS bucket. In our case, we have used China
(Shanghai) region.
- Let the remaining configuration in the configuration form to its default values.
- This bucket is getting prepared for storing the compiled model by TVM.
- Click on OK.
### Building The Container for Model Compilation
The steps below allow user to build the docker container for compiling the custom
YoloV3 and tiny yolov3 Object Detection model into the TVM Compiled model format.
The Model compilation is the most important aspect to consider when users want to
deploy the model on the low-cost, low-power embedded platform. Model compilation
reduces unwanted operations, optimizes the math ops, optimizes the model graph,
memory mapping & cache optimization etc.
#### For Alibaba trained model and storing in OSS bucket
Alibaba trained model from the bucket is taken as input and TVM compiled model in
OSS bucket.
Please follow the steps below,
- Copy the project folder from cloned project directory (on Host) to ARM system ready device using command given below,
```sh
$ scp -r <PROJECT_FOLDER_PATH> root@<ip_address_of_device>:~
```
- Login to shell of the ARM system ready device with command given below,
```sh 
$ ssh root@<ip_address_of_device>
```
- Open the ”alibaba_config.json” from <PROJECT_ROOT>/config folder of the Project Root Directory from ARM system ready device device, run the commands below in the terminal from Project Root Directory.
```sh
$ cd <PROJECT_ROOT_FOLDER>/config/
$ vi alibaba_config.json
```
- Update the configuration parameters given below in alibaba_config.json,
  - ALIBABA_ACCESS_KEY – can be found in CSV file downloaded while creating RAM.
  - AIBABA_SECRET_KEY – can be found in CSV file downloaded while creating RAM.
  - ALIBABA_REGION_ID – Give region used while creation of OSS bucket.
    - Refer the link for getting the proper region id,
    https://www.alibabacloud.com/help/en/iot-platform/latest/regions
- Make changes and save the file.
- Follow the steps below to build the container in the ARM system ready device.
- Run the commands given below from the root of the project directory from ARM system ready device.
```sh
$ cd <PROJECT_ROOT>
$ docker build -t <IMAGE_NAME>:<TAG_NAME> .
``` 
  - IMAGE_NAME, is name of the docker image.
  - TAG_NAME, is to tag the different variant of same image.
  - Users can specify the IMAGE_NAME & TAG_NAME of their choice.

` Note- This step takes 4.5 to 5 hours of time to build the image `
- The above step may take a while. Once the docker image is built, the next step is to run the container. Run the below command to run the container.

```sh
$ docker run -it --network=host <IMAGE_NAME>:<TAG_NAME> -p
alibaba_cloud -ib <INPUT_BUCKET_NAME> -ob <OUTPUT_BUCKET_NAME> -
if <INPUT_FILEPATH> -of <OUTPUT_FILEPATH>
```
- Arguments:

    -   -p: Taking model locally or from Alibaba cloud. If locally use “local” if Alibaba cloud use “alibaba_cloud”.
    - -ib: Input bucket name. Created while training the model & contains the latest trained model
    - -ob: Output bucket name. Generated by the user in the above steps.
    - -if: Path of the ONNX Model file inside the bucket. This file is generated during model training step.
    - -of: Path for TVM Compiled model to store in output bucket (Make sure that name must contain “.tar.gz” extension)
- An example command for running the docker image would be
```sh 
$ docker run --network=host \
-v $PWD/trained_model:/trained_model/
compilation:v1 -p alibaba \
-if trainedmodel0623_onnx/trained_model.onnx \
-of trainedmodel0623_onnx/compiled_model.tar.gz \
-ib greengrass-orchestration -ob greengrass-orchestration
```
- Once the execution of the docker run is complete, it generates the compiled model inside the output bucket which is given by the user in config.
- Make sure that internet speed is good.
### Compiling the trained model locally.
- TVM compilation compiles the ONNX model to TVM and stores it in container. Once user clicks on download link model is downloaded in local system.
- Copy the project folder from cloned project directory (on Host) to ARM system ready device using command given below.
```sh
$ scp -r <PROJECT_FOLDER> root@<ip_address_of_device>:~
```
- Login to shell of the ARM system ready device with command given below,
```sh 
$ ssh root@<ip_address_of_device>
```
- Follow the steps below to build the container in the ARM system ready device.
- Run the commands given below from the root of the project directory from ARM system ready device.
```sh 
$ cd <PROJECT_FOLDER>
$ docker build -t <IMAGE_NAME>:<TAG_NAME> .

```

  - IMAGE_NAME, is name of the docker image.
  - TAG_NAME, is to tag the different variant of same image.
  
- Users can specify the IMAGE_NAME & TAG_NAME(in lower case) of their
choice.

` Note: This step takes 4.5 to 5 hours of time to build the image `

The above step may take some time. Once the docker image is built, the next step is to run the container.
- Create a folder named trained_model/ and copy the trained model into that
folder.
```sh 
$ mkdir trained_model/
$ cp <TRAINED_MODEL_FILE_PATH> trained_model/
```
Run the below command to compile the trained model.
```sh
$ docker run -it --network=host –v
$PWD/trained_model/:/trained_model/ <IMAGE_NAME>:<TAG_NAME> -p
local -if <INPUT_FILENAME> -of <OUTPUT_FILENAME>
```
- Arguments:
  - -p: Taking model locally or from Alibaba cloud. If locally use “local” if Alibaba cloud use “alibaba_cloud”.
  - -if: Name of the ONNX Model file. Generated during model training step.
  - -of: Name for TVM Compiled model (Make sure that name must contain “.tar.gz” extension)
- An example command for running the docker image would be

```sh
$ docker run --network=host \
-v $PWD/trained_model:/trained_model/ compilation:v1 \
-p local -if tiny_stage2_opt.onnx -of compiled_model.tar.gz
```
- After compilation the docker container will provide us the download link. Copy and paste in browser to download the model.
- This compiled model will be used in building the inference container. 
- Copy the newly compiled model to model_data folder of inference project folder and build the
inference container.
## Compiling the model and storing it on AWS cloud
### Create IAM user
- Follow the link given below in order to setup IAM user,
https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html
### Creating Secret key and Access key for IAM user
1. Login to AWS console and navigate to IAM service console.
2. On left side menu select users under Access Management.
3. Click on the created user or preferred user.
4. Select the security credentials tab.
5. In the Access keys section, choose Create access key. If you already have two
access keys, this button is deactivated and you must delete an access key before
you can create a new one.
6. On the Access key best practices & alternatives page, choose your use case to
learn about additional options which can help you avoid creating a long-term
access key. If you determine that your use case still requires an access key,
choose Other and then choose Next.
7. (Optional) Set a description tag value for the access key. This adds a tag key-
value pair to your IAM user. This can help you identify and rotate access keys
later. The tag key is set to the access key id. The tag value is set to the access key
description that you specify. When you are finished, choose Create access key.
8. On the Retrieve access keys page, choose either Show to reveal the value of
your user's secret access key, or Download .csv file. This is your only opportunity
to save your secret access key. After you've saved your secret access key in a
secure location, choose Done.
9. Make sure that the IAM user created has below mentioned policy added.
   - AmazonS3FullAccess
   - User can add this permission by following below steps.
     - Login to AWS console and Navigate to IAM console.
     - On left side menu click on users and click on the user created.
     - Select the Add permissions under Permissions tab and click on Add permissions in tab.
     - In Permissions Options section select attach permissions directly.
     - In Permissions policy section search for S3FullAccess and select the s3 full access permission.
     - Click on next.
     - Click on Add permissions.
     - The policy will be added.

### Setting up S3 bucket
- Follow the link given below in order to setup the S3 bucket.
https://docs.aws.amazon.com/AmazonS3/latest/userguide/creating-bucket.html

### Building the docker container
Please follow the steps below to build and run the docker container
- Copy the project folder from cloned project directory (on Host) to ARM system ready device device using command given below,
```sh
$ scp -r <PROJECT_FOLDER_PATH> root@<ip_address_of_device>:~
```
- Login to shell of the ARM system ready device with command given below,
```sh 
$ ssh root@<ip_address_of_device>
```
- Open the ”aws_config.json” file from <PROJECT_ROOT>/config folder of the Project Root Directory from ARM system ready device.Run the commands below in the terminal from Project Root Directory,
```sh 
$ cd <PROJECT_ROOT_FOLDER>/config/
$ vi aws_config.json
```
- Update the configuration parameters given below in aws_config.json,
  - access_key – can be found in CSV file downloaded while creating IAM user.
  - secret_key – can be found in CSV file downloaded while creating IAM user.
  - region – Give preferred region id. The user can select the preferred region id from the link. For example Mumbai region id is ap-south-1.
  ` Note that this region id and region id when creating the s3 bucket should be same. `
- Make changes and save the file.
- Follow the steps below to build the container in the ARM system ready device.
- Run the commands given below from the root of the project directory from ARM
system ready device.
```sh 
$ cd <PROJECT_ROOT>
$ docker build -t <IMAGE_NAME>:<TAG_NAME> .
```
- IMAGE_NAME, is name of the docker image.
- TAG_NAME, is to tag the different variant of same image.
Users can specify the IMAGE_NAME & TAG_NAME of their choice.

` Note: This step takes 4.5 to 5 hours of time to build the image `
- The above step may take a while. Once the docker image is built, the next step is to run the container.
-  Run the below command to run the container.
```sh 
$ docker run -it --network=host <IMAGE_NAME>:<TAG_NAME> -p aws -
ib <INPUT_BUCKET_NAME> -ob <OUTPUT_BUCKET_NAME> -if
<INPUT_FILE_PATH> -of <OUTPUT_FILE_PATH>
```
- Arguments:
  - -p: Taking model locally or from cloud. If locally use “local”, if Alibaba cloud use “alibaba_cloud”, if AWS cloud use “aws”
  - -ib: Input bucket name. Created while training the model &
contains the latest trained model.
  - -ob: Output bucket name. Generated by the user in the above steps.
  - -if: Path of the ONNX Model file inside the bucket. This file is generated during model training step.
  - -of: Path for TVM Compiled model to store in output bucket. (Make sure that name must contain “.tar.gz” extension)

- An example command for running the docker image would be
```sh 
$ docker run --network=host \
-v $PWD/trained_model:/trained_model/
compilation:v1 -p aws \
-if trainedmodel0623_onnx/trained_model.onnx \
-of trainedmodel0623_onnx/compiled_model.tar.gz \
-ib greengrass-orchestration -ob greengrass-orchestration
```
- Once the execution of the docker run is complete, it generates the compiled model inside the output bucket which is given by the user in config.
- Make sure that internet speed is good.

## What's next,
Refer to README.md file of Video Capturing container repository.