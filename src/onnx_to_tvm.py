"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import onnx
import os
import tvm.relay as relay
from src.tar_export import export


def onnx_to_tvm(input_file_path, output_file_path):
        """
        Convert ONNX model to TVM format and save it to the specified output file or cloud service storage.

        This function takes an ONNX model stored at the given input file path, converts it to 
        TVM format using the TVM framework for cortex-a53, and saves the resulting TVM module 
        to the specified output file path or to the cloud service provider's storage like S3 or OSS.

        Parameters:
        input_file_path (str): Path to the input ONNX file containing the model to be converted.
        output_file_path (str): Path to the output file where the converted TVM module will be saved.

        Returns:
        None

        Example:
        input_path = "model.onnx"
        output_path = "model.tvm"
        onnx_to_tvm(input_path, output_path)
        """
        if (os.path.exists('compiled_tvm_model') is False):
                os.makedirs("./compiled_tvm_model")
                
        print("Input_model :", input_file_path)
        onnx_model = onnx.load(input_file_path)
        print("Loaded ONNX model")

        target = "llvm -mtriple=aarch64-linux-gnu  -mcpu=cortex-a53"
        input_name = "input_1"
        shape_dict = {input_name: (1, 416, 416, 3)}

        mod, params = relay.frontend.from_onnx(onnx_model, shape_dict)
        print("Loaded onnx model to TVM relay")

        with relay.build_config(opt_level=4):
                graph, lib, params = relay.build_module.build(mod, target, params=params)
        print("Compiled onnx model to TVM model")

        lib.export_library("./compiled_tvm_model/deploy_lib.so")
        with open("./compiled_tvm_model/deploy_graph.json", "w") as fo:
                fo.write(graph)
        with open("./compiled_tvm_model/deploy_param.params", "wb") as fo:
                fo.write(relay.save_param_dict(params))

        print('Exported the TVM model\n')
        print("Compressing the model files")

        export("./compiled_tvm_model", output_file_path)
