"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
alibaba_agent.py:
    Applying the configuration for uploading files to Alibaba Cloud
"""

import oss2
import json
import os

from src.logger import get_logger

logger = get_logger()

# Define a class for interacting with Alibaba Cloud OSS
class AlibabaAgent:
    def __init__(self):
        """
        Constructor of AlibabaAgent class.

        This function loads the cloud account configuration & open the authenticated
        connection with Alibaba cloud service for OSS storage service. The created
        OSS instance will be used for download the ONNX model and stores the compiled
        TVM model into the OSS storage so it can be served to the inference container.

        Parameters:
        None

        Returns:
        None
        """
        # Load Alibaba Cloud configuration from a JSON file
        config_path = "config/alibaba_config.json"
        if not os.path.exists(config_path):
            logger.warning("Alibaba config file is not available.")
            raise ValueError("Alibaba config file is not available.")
        
        # Load Alibaba Cloud credentials and region from the config file    
        with open(config_path) as json_data_file:
            self.alibaba_data = json.load(json_data_file)
            required_keys = ["access_key", "secret_key", "region"]
            for key in required_keys:
                if key not in self.alibaba_data:
                    logger.warning(f"Required key '{key}' not found in Alibaba config.")
                    raise ValueError(f"Required key '{key}' not found in Alibaba config.")
        
        # Authenticate and connect to the Alibaba OSS bucket    
        logger.info("Authenticating")
        self.auth = oss2.Auth(self.alibaba_data["access_key"], self.alibaba_data["secret_key"])
        logger.info("Connecting to bucket")
        
        try:
            # Create an OSS bucket object for operations
            self.bucket = oss2.Bucket(self.auth, "http://oss-"+ self.alibaba_data["region"]+".aliyuncs.com", self.alibaba_data["bucket_name"])
            logger.info("Connected to OSS Bucket %s", self.alibaba_data["bucket_name"])
        except Exception as e:
            logger.warning(f"Failed to connect to Alibaba OSS: {e}")
            raise ConnectionError(f"Failed to connect to Alibaba OSS: {e}")
    
    # Callback function to display download progress
    def percentage(self, consumed_bytes, total_bytes):
        """
        Show model download progress in percentage.
        
        Parameters:
        consumed_bytes: Total number of bytes downloaded.
        total_bytes: Total number of bytes of file being downloaded.

        Returns:
        None
        """
        if total_bytes:
            rate = int(100 * (float(consumed_bytes) / float(total_bytes)))
            logger.debug('Downloading Model %d%%', rate)

    # Download a file from Alibaba OSS
    def download(self, file, save_file_path):
        """
        Downloads the Fused ONNX model from OSS bucket and stores to the path
        specified in the parameters.
        
        Parameters:
        file: Name of file to be downloaded from OSS bucket.
        save_file_path: Folder path to save downloaded model.

        Returns:
        None
        """
        try:
            logger.info("Downloading the fused ONNX model...")
            # Fetch metadata about the OSS object
            meta_data = self.bucket.head_object(file)
            total_length = int(meta_data.get('ContentLength', 0))
            if total_length <= 0:
                logger.warning("Invalid ContentLength received from OSS.")
                raise ValueError("Invalid ContentLength received from OSS.")
            
            logger.info("Total Size of Model: %d MB", round(total_length/1024/1024))

            # Download the OSS object to a local file
            with open(save_file_path, "wb") as f:
                self.bucket.get_object(file, progress_callback=self.percentage, response_callback=f.write)
                
            logger.info("Download complete.")
        except Exception as e:
            logger.warning(f"Error during Alibaba OSS download: {e}")
            raise RuntimeError(f"Error during Alibaba OSS download: {e}")
    
    # Upload a file to Alibaba OSS
    def upload(self, compiled_file_path, save_file_path_to_oss):
        """
        Upload the TVM compiled model to the specified Alibaba OSS bucket.
        
        Parameters:
        compiled_file_path: Path of model where compiled model is save.
        save_file_path_to_oss: Path for TVM Model to be saved in the Alibaba OSS.

        Returns:
        None
        """
        try:
            # Get the file size for uploading
            file_size = os.stat(compiled_file_path).st_size
            if file_size <= 0:
                logger.warning("Invalid file size for upload.")
                raise ValueError("Invalid file size for upload.")
            
            logger.info("Uploading the TVM-converted model to OSS bucket...")
            
            # Upload the local file to the OSS bucket
            with open(compiled_file_path, "rb") as f:
                self.bucket.put_object(save_file_path_to_oss, f)
            
            logger.info("TVM converted Model uploaded to OSS bucket")
        except Exception as e:
            logger.warning(f"Error during Alibaba OSS upload: {e}")
            raise RuntimeError(f"Error during Alibaba OSS upload: {e}")
