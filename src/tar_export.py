"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

import tarfile
import os

def export(folder, tarname):
    """
	Create a .tar.gz

	Args:
		folder (string): Location of directory to compress
		tarname (string): What to name the compressed folder
	Returns:
		None
	"""
    # Check if the specified folder exists and is a directory
    if not os.path.exists(folder) or not os.path.isdir(folder):
        print(f"The specified folder '{folder}' is not available.")
        return
    
    # Create a tar file for writing in gzip format
    file = tarfile.open(tarname, "w|gz")
    
    # Get a list of items (files and subfolders) in the specified folder
    o = os.listdir(folder)
    
    # Add each item to the tar file
    for i in o:
        print(f"Adding: {i}")
        file.add(os.path.join(folder, i))
    
    # Close the tar file
    file.close()
    
    # Check if the tar file was successfully created
    if os.path.exists(tarname):
        print(f"Tar file '{tarname}' was successfully created.")
    else:
        print("Failed to create the tar file.")

