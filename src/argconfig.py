"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import argparse

def parse_args(parser):
    parser.add_argument("-tmp" , "--trained_model_path", required=True, help="Path to the input ONNX model")
    parser.add_argument("-cmp" , "--compiled_model_path", required=True, help="Path to the output compiled model")    
    parser.add_argument("-p", "--platform", required=True, help="Compiling and storing model locally or on Alibaba Cloud")
    parser.add_argument("-ib", "--input_bucket",help="Input OSS bucket name")
    parser.add_argument("-ob", "--output_bucket", help="Output OSS bucket name")
    parser.add_argument("-if", "--input_file", required=True, help="Input file name")
    parser.add_argument("-of", "--output_file", required=True, help="Output file name")
    args = vars(parser.parse_args())
    return args

def get_args():
    parser = argparse.ArgumentParser()
    args = parse_args(parser)
    return args