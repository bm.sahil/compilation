from src.alibaba_agent import AlibabaAgent
from src.aws_agent import AwsAgent
from src.onnx_to_tvm import onnx_to_tvm
from src.logger import get_logger


def compile_model(args):
    """
    Compiles a machine learning model based on the provided arguments and 
    saves in resceptive cloud or local server.

    Parameters:
    args (dict): A dictionary containing cloud configuration and required parameters for model compilation.
        It should include the following keys:
        - trained_model_path: Path to the input ONNX model
        - compiled_model_path: Path to the output compiled model
        - platform: Compiling and storing model locally or on Alibaba Cloud
        - input_bucket: Input OSS bucket name
        - output_bucket: Output OSS bucket name
        - input_file: Input file name
        - output_file: Output file name
        
    Returns:
    None
    """
    logger = get_logger
    if args["platform"] == "alibaba_cloud":
        cloud_agent = AlibabaAgent()
        try:
            # Download ONNX model from Alibaba Cloud, convert to TVM, and upload compiled model
            logger.info("Starting Alibaba Cloud operations...")
            cloud_agent.download(args["input_bucket"], args["input_file"], args["trained_model_path"])
            onnx_to_tvm(args["trained_model_path"], args["compiled_model_path"])
            cloud_agent.upload(args["output_bucket"], args["compiled_model_path"], args["output_file"])
            logger.info("Alibaba Cloud operations completed successfully.")
        except Exception as e:
            logger.warning("An error occurred during Alibaba Cloud operations:", exc_info=True)
            
    elif args["platform"] == "aws":
        cloud_agent = AwsAgent()
        try:
            logger.info("Agent selected: AWS")
            # Download ONNX model from AWS, convert to TVM, and upload compiled model
            logger.info("Starting AWS operations...")
            cloud_agent.download_from_s3(args["input_bucket"], args["input_file"], args["trained_model_path"])
            logger.info("Downloaded model... Starting compilation...")
            onnx_to_tvm(args["trained_model_path"], args["compiled_model_path"])
            cloud_agent.upload_to_s3(args["output_bucket"], args["output_file"], args["compiled_model_path"])
            logger.info("AWS operations completed successfully.")
        except Exception as e:
            logger.warning("An error occurred during AWS operations:", exc_info=True)
        
    elif args["platform"] == "local":
        try:
            logger.info("Platform selected: local")
            # Convert ONNX model to TVM and save it locally
            onnx_to_tvm(args["input_file"], args["output_file"])
            logger.info("Local operations completed successfully.")
        except Exception as e:
            logger.warning("An error occurred during local operations:", exc_info=True)
        
    else:
        logger.warning("Invalid option...")
